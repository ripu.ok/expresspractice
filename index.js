const express = require("express");
const bodyParser= require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({}));

app.get("/",function(req,res){
    res.sendFile(__dirname+"/index.html");
});

app.post("/",function(req,res){
    var height = Number(req.body.num1);
    var weight = Number(req.body.num2);  
    
    var result = weight/(height*height);
    res.send('Your height: '+height+'meters , weight: '+weight+' kg and your BMI is: '+ result );
        

})

app.listen(5000,function(){
    console.log("Server Running at port 5000")
});